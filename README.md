## About:

RecBox Studio Backup is a simple tool made for backing up recording session and DAW config files with rsync help.

![studiobackup](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-studio-backup.gif)

There is no Cancel button in progress dialog, because I can't get working `--auto-kill` flag. I've already start working on GTK3 version, so if I don't find time, to fix it, fully functional dialog will be available in [GTK version](https://gitlab.com/recbox/recbox-toolkit-gtk).

## Installation:

#### Manjaro:
```
git clone https://gitlab.com/recbox/recbox-toolkit-bash/recbox-studio-backup.git
cd recbox-studio-backup
sudo cp -r sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity rsync
```

## Translation
Directory containing translations can be found here:
* /usr/share/recbox-studio-backup
* /usr/share/applications/recbox-studio-backup.desktop

#### Translations:
* [x] en_US
* [x] pl_PL

## Translation contribution:
* type `echo $LANG` in terminal to check language used by your system
* copy `en_US.trans` file and rename it (replace `.utf8` with `.trans`)
* replace text under quotation marks
