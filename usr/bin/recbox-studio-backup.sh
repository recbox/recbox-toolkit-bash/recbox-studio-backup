#!/usr/bin/env bash

VER="1.1"

# Translation
source /usr/bin/recbox-studio-backup_lang.sh

CONFIG_PATH="$HOME/.config/recbox-studio-backup-settings/config"
ICON_PATH="/usr/share/icons/hicolor/scalable/apps/recbox-studio-backup"

create_new_config() {
touch "$CONFIG_PATH"
echo -e "studio_session_dir=none
studio_backup_dir=none
daw_config_dir=none
daw_backup_dir=none" > "$CONFIG_PATH"
}

    if [[ ! -d $HOME/.config/recbox-studio-backup-settings ]]; then
        mkdir "$HOME/.config/recbox-studio-backup-settings"
        create_new_config
    elif [[ ! -f $CONFIG_PATH ]]; then
        create_new_config
    elif [[ ! -s $CONFIG_PATH ]]; then
        create_new_config
    fi

SETTINGS_TEXT="$SETTINGS_HEADLINE_TEXT \
\n\n<b>$SETTINGS_SESSION_DIR:</b>\n$(grep -w 'studio_session_dir' "$CONFIG_PATH" | awk -F '=' '{print $2}') \
\n\n<b>$SETTINGS_SESSION_BACKUP_DIR:</b>\n$(grep -w 'studio_backup_dir' "$CONFIG_PATH" | awk -F '=' '{print $2}') \
\n\n<b>$SETTINGS_DAW_DIR:</b>\n$(grep -w 'daw_config_dir' "$CONFIG_PATH" | awk -F '=' '{print $2}') \
\n\n<b>$SETTINGS_DAW_BACKUP_DIR:</b>\n$(grep -w 'daw_backup_dir' "$CONFIG_PATH" | awk -F '=' '{print $2}')"
PICK_COLUMN_TITLE="        "
SEPARATOR=""

STUDIO_SESSION_DIR=$(grep -w 'studio_session_dir' "$CONFIG_PATH" | awk -F '=' '{print $2}')
STUDIO_BACKUP_DIR=$(grep -w 'studio_backup_dir' "$CONFIG_PATH" | awk -F '=' '{print $2}')
DAW_CONFIG_DIR=$(grep -w 'daw_config_dir' "$CONFIG_PATH" | awk -F '=' '{print $2}')
DAW_BACKUP_DIR=$(grep -w 'daw_backup_dir' "$CONFIG_PATH" | awk -F '=' '{print $2}')

ZEN_MAIN_MENU="zenity --list --radiolist --width=640 --height=530 --window-icon=$ICON_PATH"
PROGRESS_BOX="zenity --progress --pulsate --no-cancel --width=520 --height=120 --window-icon=$ICON_PATH"
INFO_WIDGET="zenity --info  --width=300 --window-icon=$ICON_PATH --icon-name=recbox-studio-backup"
ERROR_WIDGET="zenity --error --width=400 --window-icon=$ICON_PATH --icon-name=recbox-studio-backup"

rsync_session_to_backup() {
    if [[ -d $STUDIO_BACKUP_DIR ]] && [[ -d $STUDIO_SESSION_DIR ]]; then
        rsync -uvr --delete "$STUDIO_SESSION_DIR"/ "$STUDIO_BACKUP_DIR"/ | $PROGRESS_BOX --title="$SETTINGS_OPTION_1" --text="<b>From:</b>\n$(echo "$STUDIO_SESSION_DIR" | cut -d "/" -f 2-) \n\n<b>to:</b>\n$(echo "$STUDIO_BACKUP_DIR" | cut -d "/" -f 2-)"
    else
        $ERROR_WIDGET --title="$WIDGET_RSYNC_ERROR_TITLE" --text="$WIDGET_RSYNC_ERROR_TEXT"
    fi
}

rsync_backup_to_session() {
    if [[ -d $STUDIO_BACKUP_DIR ]] && [[ -d $STUDIO_SESSION_DIR ]]; then
        rsync -uvr --delete "$STUDIO_BACKUP_DIR"/ "$STUDIO_SESSION_DIR"/ | $PROGRESS_BOX --title="$SETTINGS_OPTION_3" --text="<b>From:</b>\n$(echo "$STUDIO_BACKUP_DIR" | cut -d "/" -f 2-) \n\n<b>to:</b>\n$(echo "$STUDIO_SESSION_DIR" | cut -d "/" -f 2-)"
    else
        $ERROR_WIDGET --title="$WIDGET_RSYNC_ERROR_TITLE" --text="$WIDGET_RSYNC_ERROR_TEXT"
    fi
}

rsync_daw_to_backup() {
    if [[ -d $DAW_CONFIG_DIR ]] && [[ -d $DAW_BACKUP_DIR ]]; then
        rsync -uvr --delete "$DAW_CONFIG_DIR"/ "$DAW_BACKUP_DIR"/ | $PROGRESS_BOX --title="$SETTINGS_OPTION_2" --text="<b>From:</b>\n$(echo "$DAW_CONFIG_DIR" | cut -d "/" -f 2-) \n\n<b>to:</b>\n$(echo "$DAW_BACKUP_DIR" | cut -d "/" -f 2-)"
    else
        $ERROR_WIDGET --title="$WIDGET_RSYNC_ERROR_TITLE" --text="$WIDGET_RSYNC_ERROR_TEXT"
    fi
}

rsync_backup_to_daw() {
    if [[ -d $DAW_CONFIG_DIR ]] && [[ -d $DAW_BACKUP_DIR ]]; then
        rsync -uvr --delete "$DAW_BACKUP_DIR"/ "$DAW_CONFIG_DIR"/ | $PROGRESS_BOX --title="$SETTINGS_OPTION_4" --text="<b>From: </b>\n$(echo "$DAW_BACKUP_DIR" | cut -d "/" -f 2-) \n\n<b>to:</b>\n$(echo "$DAW_CONFIG_DIR" | cut -d "/" -f 2-)"
    else
        $ERROR_WIDGET --title="$WIDGET_RSYNC_ERROR_TITLE" --text="$WIDGET_RSYNC_ERROR_TEXT"
    fi
}

MAIN_BOX=$($ZEN_MAIN_MENU --title="$SETTINGS_TITLE" --text="$SETTINGS_TEXT" \
--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --column="$PICK_COLUMN_TITLE" \
--column="$SETTINGS_OPT_COLUMN_TITLE" --column="$SEPARATOR" --column="$SETTINGS_DESC_COLUMN_TITLE" \
--column="$SEPARATOR" \
    TRUE "$SETTINGS_OPTION_1" "$SPACE" "$SETTINGS_DESCRIPTION_1" "$SPACE" \
    FALSE "$SETTINGS_OPTION_2" "$SPACE" "$SETTINGS_DESCRIPTION_2" "$SPACE" \
    FALSE "$SETTINGS_OPTION_3" "$SPACE" "$SETTINGS_DESCRIPTION_3" "$SPACE" \
    FALSE "$SETTINGS_OPTION_4" "$SPACE" "$SETTINGS_DESCRIPTION_4" "$SPACE" \
    FALSE "$SETTINGS_OPTION_5" "$SPACE" "$SETTINGS_DESCRIPTION_5" "$SPACE" \
    FALSE "$SETTINGS_OPTION_6" "$SPACE" "$SETTINGS_DESCRIPTION_6" "$SPACE" \
    FALSE "$SETTINGS_OPTION_7" "$SPACE" "$SETTINGS_DESCRIPTION_7" "$SPACE" \
    FALSE "$SETTINGS_OPTION_8" "$SPACE" "$SETTINGS_DESCRIPTION_8" "$SPACE" \
    FALSE "$SETTINGS_OPTION_9" "$SPACE" "$SETTINGS_DESCRIPTION_9" "$SPACE")

    if [[ $MAIN_BOX == "$SETTINGS_OPTION_1" ]]; then
        if [[ $STUDIO_SESSION_DIR == "none" ]]; then
            $INFO_WIDGET --title="$SETTINGS_TITLE" --text="$WIDGET_STUDIO_SESSION_DIR"
            exec recbox-studio-backup.sh
        elif [[ $STUDIO_BACKUP_DIR == "none" ]]; then
            $INFO_WIDGET --title="$SETTINGS_TITLE" --text="$WIDGET_SESSION_BACKUP_DIR"
            exec recbox-studio-backup.sh
        else
            rsync_session_to_backup && exec recbox-studio-backup.sh || $ERROR_WIDGET --title="$WIDGET_RSYNC_ERROR_TITLE" --text="$WIDGET_RSYNC_ERROR_TEXT"
        fi
    elif [[ $MAIN_BOX == "$SETTINGS_OPTION_2" ]]; then
        if [[ $DAW_CONFIG_DIR == "none" ]]; then
            $INFO_WIDGET --title="$SETTINGS_TITLE" --text="$WIDGET_DAW_DIR"
            exec recbox-studio-backup.sh
        elif [[ $DAW_BACKUP_DIR == "none" ]]; then
            $INFO_WIDGET --title="$SETTINGS_TITLE" --text="$WIDGET_DAW_BACKUP_DIR"
            exec recbox-studio-backup.sh
        else
            rsync_daw_to_backup && exec recbox-studio-backup.sh || $ERROR_WIDGET --title="$WIDGET_RSYNC_ERROR_TITLE" --text="$WIDGET_RSYNC_ERROR_TEXT"
        fi
    elif [[ $MAIN_BOX == "$SETTINGS_OPTION_3" ]]; then
        if [[ $STUDIO_BACKUP_DIR == "none" ]]; then
            $INFO_WIDGET --title="$SETTINGS_TITLE" --text="$WIDGET_SESSION_BACKUP_DIR"
            exec recbox-studio-backup.sh
        elif [[ $STUDIO_SESSION_DIR == "none" ]]; then
            $INFO_WIDGET --title="$SETTINGS_TITLE" --text="$WIDGET_STUDIO_SESSION_DIR"
            exec recbox-studio-backup.sh
        else
            rsync_backup_to_session && exec recbox-studio-backup.sh || $ERROR_WIDGET --title="$WIDGET_RSYNC_ERROR_TITLE" --text="$WIDGET_RSYNC_ERROR_TEXT"
        fi
    elif [[ $MAIN_BOX == "$SETTINGS_OPTION_4" ]]; then
        if [[ $DAW_BACKUP_DIR == "none" ]]; then
            $INFO_WIDGET --title="$SETTINGS_TITLE" --text="$WIDGET_DAW_BACKUP_DIR"
            exec recbox-studio-backup.sh
        elif [[ $DAW_CONFIG_DIR == "none" ]]; then
            $INFO_WIDGET --title="$SETTINGS_TITLE" --text="$WIDGET_DAW_DIR"
            exec recbox-studio-backup.sh
        else
            rsync_backup_to_daw && exec recbox-studio-backup.sh || $ERROR_WIDGET --title="$WIDGET_RSYNC_ERROR_TITLE" --text="$WIDGET_RSYNC_ERROR_TEXT"
        fi
    elif [[ $MAIN_BOX == "$SETTINGS_OPTION_5" ]]; then
        SEL_BOX=$(zenity --file-selection --title="Choose Studio Session directory" --directory | sed 's|/|\\/|g')
        if [[ $SEL_BOX == "" ]]; then
            exec recbox-studio-backup.sh
        else
            sed -i "/studio_session_dir/ s/$(grep -w studio_session_dir "$CONFIG_PATH" | awk -F '=' '{print $2}' | sed 's|/|\\/|g')/$SEL_BOX/" "$CONFIG_PATH"
            exec recbox-studio-backup.sh
        fi
    elif [[ $MAIN_BOX == "$SETTINGS_OPTION_6" ]]; then
        SEL_BOX=$(zenity --file-selection --title="Choose backup directory for Studio Session" --directory | sed 's|/|\\/|g')
        if [[ $SEL_BOX == "" ]]; then
            exec recbox-studio-backup.sh
        else
            sed -i "/studio_backup_dir/ s/$(grep -w studio_backup_dir "$CONFIG_PATH" | awk -F '=' '{print $2}' | sed 's|/|\\/|g')/$SEL_BOX/" "$CONFIG_PATH"
            exec recbox-studio-backup.sh
        fi
    elif [[ $MAIN_BOX == "$SETTINGS_OPTION_7" ]]; then
        SEL_BOX=$(zenity --file-selection --title="Choose DAW configs directory" --directory | sed 's|/|\\/|g')
        if [[ $SEL_BOX == "" ]]; then
            exec recbox-studio-backup.sh
        else
            sed -i "/daw_config_dir/ s/$(grep -w daw_config_dir "$CONFIG_PATH" | awk -F '=' '{print $2}' | sed 's|/|\\/|g')/$SEL_BOX/" "$CONFIG_PATH"
            exec recbox-studio-backup.sh
        fi
    elif [[ $MAIN_BOX == "$SETTINGS_OPTION_8" ]]; then
        SEL_BOX=$(zenity --file-selection --title="Choose backup directory for DAW" --directory | sed 's|/|\\/|g')
        if [[ $SEL_BOX == "" ]]; then
            exec recbox-studio-backup.sh
        else
            sed -i "/daw_backup_dir/ s/$(grep -w daw_backup_dir "$CONFIG_PATH" | awk -F '=' '{print $2}' | sed 's|/|\\/|g')/$SEL_BOX/" "$CONFIG_PATH"
            exec recbox-studio-backup.sh
        fi
    elif [[ $MAIN_BOX == "$SETTINGS_OPTION_9" ]]; then
        zenity --info --no-wrap --icon-name="recbox-studio-backup" --window-icon="$ICON_PATH" \
        --title="$WIDGET_ABOUT_TITLE" --text="<b>Version: $VER</b>\n\n$WIDGET_ABOUT_TEXT"
        exec recbox-studio-backup.sh
    fi
